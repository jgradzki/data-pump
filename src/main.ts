require('module-alias/register');

import * as dotenv from 'dotenv-safe';

import { ConfigService } from '@/services/ConfigService';
import { LoggerService } from '@/services/LoggerService';
import { ElasticsearchService } from '@/services/ElasticsearchService';
import { MaterialPricesService } from '@/services/MaterialPricesService';

dotenv.config({ allowEmptyValues: true });

async function runApp() {
  const configService = new ConfigService();
  const loggerService = new LoggerService();
  const elasticsearchService = new ElasticsearchService(
    loggerService,
    configService,
  );
  const materialPricesService = new MaterialPricesService(
    loggerService,
    elasticsearchService,
    configService,
  );

  await materialPricesService.run();

  loggerService.log('done');
}

runApp().catch(console.error);
