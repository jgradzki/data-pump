export class LoggerService {
  public log(data: any) {
    console.log(JSON.stringify(data, null, 2));
  }
  public error(data: any) {
    console.error(JSON.stringify(data, null, 2));
  }
  public debug(data: any) {}
}
