import fetch from 'node-fetch';

import { LoggerService } from '@/services/LoggerService';
import { ElasticsearchService } from '@/services/ElasticsearchService';
import { ConfigService } from '@/services/ConfigService';

import { RainPricesInterface } from '@/interfaces/fioApi/RainPricesInterface';
import { MaterialPriceSnapshot } from '@/interfaces/data/MaterialPricesInterface';

export class MaterialPricesService {
  constructor(
    protected readonly loggerService: LoggerService,
    protected readonly elasticsearchService: ElasticsearchService,
    protected readonly configService: ConfigService,
  ) {}

  async run(): Promise<void> {
    try {
      this.loggerService.log('Fetching...');
      const materials = await this.fetchMaterials();
      this.loggerService.log(`Fetched ${materials.length} materials prices`);

      await this.updateCurrent(materials);
      await this.updateSnapshots(materials);
    } catch (e) {
      this.loggerService.error(e);
    }
  }

  async fetchMaterials(): Promise<RainPricesInterface[]> {
    const { apiUrl } = this.configService.getGeneralConfig();

    const response = await fetch(`${apiUrl}/rain/prices`, {
      timeout: 30000,
    });

    return (await response.json()) as RainPricesInterface[];
  }

  async updateSnapshots(materials: RainPricesInterface[]): Promise<void> {
    try {
      const esClient = await this.elasticsearchService.getClient();
      const {
        materialPricesSnapshotsInterval,
      } = this.configService.getGeneralConfig();
      const {
        materialPricesSnapshotsIndexName,
      } = this.configService.getElasticConfig();
      const indexStatus = await esClient.indices.exists({
        index: materialPricesSnapshotsIndexName,
      });

      this.loggerService.log('Updating material prices snapshots');

      if (indexStatus.statusCode !== 200) {
        await this.createIndex(materialPricesSnapshotsIndexName);
      }

      const lastDocuments = await this.elasticsearchService.getIndex<MaterialPriceSnapshot>(
        materialPricesSnapshotsIndexName,
        {
          body: {
            size: 2,
            sort: { '@timestamp': 'desc' },
            query: {
              bool: {
                filter: [
                  {
                    term: {
                      'Ticker.keyword': 'H2O',
                    },
                  },
                ],
              },
            },
          },
        },
      );

      if (lastDocuments?.hits?.hits?.[0]?._source?.['@timestamp']) {
        const diffMargin = 2 * 60 * 1000;
        const lastSnapshotDiff =
          Date.now() - lastDocuments.hits.hits[0]._source['@timestamp'];

        // Checking if last snapshot is not too old
        if (lastSnapshotDiff + diffMargin < materialPricesSnapshotsInterval) {
          if (lastDocuments?.hits?.hits?.[1]?._source?.['@timestamp']) {
            const penultimateSnapshotDiff =
              Date.now() - lastDocuments.hits.hits[1]._source['@timestamp'];

            // Checking if penultimate snapshot is older than interval
            if (
              penultimateSnapshotDiff + diffMargin <
              materialPricesSnapshotsInterval
            ) {
              // Snapshots not older than interval, updating last ones

              this.loggerService.log('Updating last material prices snapshots');

              for (const material of materials) {
                this.loggerService.log(`Update ${material.Ticker} snapshot`);

                await this.processMaterialPrices(
                  materialPricesSnapshotsIndexName,
                  material,
                  {
                    forceInsert: false,
                  },
                );
              }

              return;
            }
          }
        } else {
          this.loggerService.log('Last material prices snapshots old');
        }
      }

      this.loggerService.log('Inserting new material prices snapshots');

      for (const material of materials) {
        this.loggerService.log(`Index ${material.Ticker} snapshot`);

        await this.processMaterialPrices(
          materialPricesSnapshotsIndexName,
          material,
          {
            forceInsert: true,
          },
        );
      }
    } catch (e) {
      this.loggerService.error(e);
    }
  }

  async updateCurrent(materials: RainPricesInterface[]): Promise<void> {
    try {
      const esClient = await this.elasticsearchService.getClient();
      const { materialPricesIndexName } = this.configService.getElasticConfig();

      this.loggerService.log('Updating current material prices');

      const indexStatus = await esClient.indices.exists({
        index: materialPricesIndexName,
      });

      if (indexStatus.statusCode !== 200) {
        await this.createIndex(materialPricesIndexName);
      }

      for (const material of materials) {
        this.loggerService.log(`Index ${material.Ticker}`);

        await this.processMaterialPrices(materialPricesIndexName, material, {
          forceInsert: false,
        });
      }
    } catch (e) {
      this.loggerService.error(e);
    }
  }

  private async processMaterialPrices(
    indexName: string,
    material: RainPricesInterface,
    options: { forceInsert?: boolean } = {},
  ): Promise<void> {
    const document = {
      ...material,
      '@timestamp': Date.now(),
    };
    let row;

    if (!options.forceInsert) {
      row = await this.elasticsearchService.getIndex(indexName, {
        body: {
          size: 1,
          sort: { '@timestamp': 'desc' },
          query: {
            bool: {
              filter: [
                {
                  term: { ['Ticker.keyword']: material.Ticker },
                },
              ],
            },
          },
        },
      });
    }

    if (row?.hits?.hits?.length) {
      this.loggerService.log('...updating...');

      await this.elasticsearchService.update({
        index: indexName,
        id: row.hits.hits[0]._id,
        body: { doc: document },
      });
    } else {
      this.loggerService.log('...inserting...');
      await this.elasticsearchService.index({
        index: indexName,
        body: document,
      });
    }
  }

  async createIndex(indexName: string): Promise<void> {
    const esClient = await this.elasticsearchService.getClient();

    this.loggerService.log(
      `Creating material-prices index with name "${indexName}"...`,
    );

    await esClient.indices.create({
      index: indexName,
      body: {
        mappings: {
          properties: {
            'AI1-AskAmt': {
              type: 'long',
            },
            'AI1-AskAvail': {
              type: 'long',
            },
            'AI1-AskPrice': {
              type: 'long',
            },
            'AI1-Average': {
              type: 'float',
            },
            'AI1-BidAmt': {
              type: 'long',
            },
            'AI1-BidAvail': {
              type: 'long',
            },
            'AI1-BidPrice': {
              type: 'float',
            },
            'CI1-AskAmt': {
              type: 'long',
            },
            'CI1-AskAvail': {
              type: 'long',
            },
            'CI1-AskPrice': {
              type: 'long',
            },
            'CI1-Average': {
              type: 'float',
            },
            'CI1-BidAmt': {
              type: 'long',
            },
            'CI1-BidAvail': {
              type: 'long',
            },
            'CI1-BidPrice': {
              type: 'long',
            },
            'IC1-AskAmt': {
              type: 'long',
            },
            'IC1-AskAvail': {
              type: 'long',
            },
            'IC1-AskPrice': {
              type: 'long',
            },
            'IC1-Average': {
              type: 'float',
            },
            'IC1-BidAmt': {
              type: 'long',
            },
            'IC1-BidAvail': {
              type: 'long',
            },
            'IC1-BidPrice': {
              type: 'long',
            },
            MMBuy: {
              type: 'long',
            },
            MMSell: {
              type: 'long',
            },
            'NC1-AskAmt': {
              type: 'long',
            },
            'NC1-AskAvail': {
              type: 'long',
            },
            'NC1-AskPrice': {
              type: 'long',
            },
            'NC1-Average': {
              type: 'float',
            },
            'NC1-BidAmt': {
              type: 'long',
            },
            'NC1-BidAvail': {
              type: 'long',
            },
            'NC1-BidPrice': {
              type: 'float',
            },
            Ticker: {
              type: 'text',
              fields: {
                keyword: {
                  type: 'keyword',
                  ignore_above: 256,
                },
              },
            },
            '@timestamp': {
              type: 'date',
            },
          },
        },
      },
    });
  }
}
