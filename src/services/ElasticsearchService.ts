import { Client, RequestParams } from '@elastic/elasticsearch';
import {
  ApiResponse,
  Context,
  RequestBody,
  TransportRequestOptions,
} from '@elastic/elasticsearch/lib/Transport';
import fastSafeStringify from 'fast-safe-stringify';

import { LoggerService } from '@/services/LoggerService';
import { ConfigService } from '@/services/ConfigService';

import { SearchResponse, CountResponse } from '@/interfaces/es';
import { ElasticConfigInterface } from '@/interfaces/ElasticConfigInterface';

export class ElasticsearchService {
  private client: Client;
  private config: ElasticConfigInterface;

  constructor(
    private readonly logger: LoggerService,
    private readonly configService: ConfigService,
  ) {
    this.config = configService.getElasticConfig();
  }

  /**
   * Get client. If client is undefined calls createClient.
   * @returns {Promise<Client>}
   */
  async getClient(): Promise<Client> {
    if (!this.client) {
      await this.createClient();
    }

    return this.client;
  }

  /**
   * Creates new elasticsearch client.
   */
  // eslint-disable-next-line @typescript-eslint/require-await
  public async createClient(): Promise<void> {
    try {
      this.client = new Client({
        auth: {
          username: this.config.user,
          password: this.config.password,
        },
        node: `${this.config.host}:${this.config.port}`,
        requestTimeout: this.config.requestTimeout,
        agent: {
          maxSockets: this.config.maxSockets,
          keepAlive: true,
        },
      });
    } catch (e) {
      this.logger.error(fastSafeStringify(e));
      throw e;
    }
  }

  /**
   * Close and remove current client instance.
   */
  public async closeClient(): Promise<void> {
    if (this.client) {
      await this.client.close();
    }

    this.client = undefined;
  }

  /**
   * Get elements count by index. Performs elasticsearch count method.
   * @param {string} index
   * @param {RequestParams.Count} options
   * @param requestOptions
   * @returns {Promise<CountResponse>}
   */
  async countIndex(
    index: string | string[],
    options: RequestParams.Count = {},
    requestOptions: TransportRequestOptions = {},
  ): Promise<CountResponse> {
    try {
      const body = {
        index,
        ...options,
      };

      this.logRequest(body, requestOptions);

      return (
        await (await this.getClient()).count<
          CountResponse,
          RequestParams.Count['body']
        >(body, requestOptions)
      ).body;
    } catch (e) {
      this.logger.error(fastSafeStringify(e));
      throw e;
    }
  }

  /**
   * Get Cat API.
   * @returns {Promise<Client.cat>}
   */
  async getCat(): Promise<Client['cat']> {
    return (await this.getClient()).cat;
  }

  /**
   * Get elements by index. Performs elasticsearch search method.
   * @param {string} index
   * @param {RequestParams.Search} options
   * @param requestOptions
   * @returns {Promise<SearchResponse<T>>}
   */
  async getIndex<T>(
    index: string | string[],
    options: RequestParams.Search = {},
    requestOptions: TransportRequestOptions = {},
  ): Promise<SearchResponse<T>> {
    try {
      const body = {
        index,
        rest_total_hits_as_int: true,
        ...options,
      };

      this.logRequest(body, requestOptions);

      return (
        await (await this.getClient()).search<
          SearchResponse<T>,
          RequestParams.Search['body']
        >(body, requestOptions)
      ).body;
    } catch (e) {
      this.logger.error(fastSafeStringify(e));
      throw e;
    }
  }

  /**
   * Index data
   *
   * @param params
   * @param options
   */
  async index<
    TResponse = Record<string, any>,
    TRequestBody extends RequestBody = Record<string, any>,
    TContext = Context
  >(
    params?: RequestParams.Index<TRequestBody>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<TResponse, TContext>> {
    return (await this.getClient()).index(params, options);
  }

  async update<
    TResponse = Record<string, any>,
    TRequestBody extends RequestBody = Record<string, any>,
    TContext = Context
  >(
    params?: RequestParams.Update<TRequestBody>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<TResponse, TContext>> {
    return (await this.getClient()).update(params, options);
  }

  private logRequest(
    body: RequestParams.Search,
    requestOptions: TransportRequestOptions,
  ) {
    if (
      process.env.LOGGER_LEVEL === 'debug' &&
      process.env.NODE_ENV === 'development'
    ) {
      // eslint-disable-next-line no-console
      console.log(
        JSON.stringify(
          {
            body,
            requestOptions,
          },
          null,
          2,
        ),
      );
    } else {
      this.logger.debug(
        fastSafeStringify({
          body,
          requestOptions,
        }),
      );
    }
  }
}
