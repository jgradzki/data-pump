import { ElasticConfigInterface } from '@/interfaces/ElasticConfigInterface';
import { GeneralConfigInterface } from '@/interfaces/GeneralConfigInterface';

export class ConfigService {
  private config: {
    elastic?: ElasticConfigInterface;
    general?: GeneralConfigInterface;
  } = {};

  constructor() {
    this.loadGeneralConfig(process.env);
    this.loadElasticConfig(process.env);
  }

  public getGeneralConfig(): GeneralConfigInterface {
    return this.config.general;
  }

  public getElasticConfig(): ElasticConfigInterface {
    return this.config.elastic;
  }

  private loadGeneralConfig(data: any): void {
    this.config.general = {
      apiUrl: data.API_URL,
      materialPricesSnapshotsInterval:
        Number(data.MATERIAL_PRICES_SNAPSHOTS_INTERVAL) * 60 * 60 * 1000 ||
        43200000, // 12h
    };
  }

  private loadElasticConfig(data: any): void {
    this.config.elastic = {
      port: data.ELASTIC_PORT ? parseInt(data.ELASTIC_PORT, 10) : 9200,
      host: data.ELASTIC_HOST || 'http://localhost',
      user: data.ELASTIC_USER,
      password: data.ELASTIC_PASSWORD,
      materialPricesIndexName:
        data.MATERIAL_PRICES_INDEX_NAME || 'material-prices',
      materialPricesSnapshotsIndexName:
        data.MATERIAL_PRICES_SNAPSHOTS_INDEX_NAME ||
        'material-prices-snapshots',
      maxSockets: data.ELASTIC_MAX_SOCKETS
        ? parseInt(data.ELASTIC_MAX_SOCKETS, 10)
        : 2,
      requestTimeout: data.ELASTIC_REQUEST_TIMEOUT
        ? parseInt(data.ELASTIC_REQUEST_TIMEOUT, 10)
        : 3000,
    };

    if (
      !this.config.elastic.host.startsWith('http://') &&
      !this.config.elastic.host.startsWith('https://')
    ) {
      this.config.elastic.host = `http://${this.config.elastic.host}`;
    }
  }
}
