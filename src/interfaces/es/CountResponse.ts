import { ShardsResponse } from '@/interfaces/es/ShardsResponse';

export interface CountResponse {
  count: number;
  _shards: ShardsResponse;
}
