import { ShardsResponse } from '@/interfaces/es/ShardsResponse';
import { SearchHitInterface } from '@/interfaces/es/SearchHitInterface';

export interface SearchResponse<T, AGGREGATIONS = any> {
  took: number;
  timed_out: boolean;
  _scroll_id?: string;
  _shards: ShardsResponse;
  hits: {
    total: number;
    max_score: number;
    hits: SearchHitInterface<T>[];
  };
  aggregations?: AGGREGATIONS;
}
