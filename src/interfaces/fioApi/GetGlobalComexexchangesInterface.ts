export interface GetGlobalComexexchangesInterface {
  ExchangeId: string;
  ExchangeName: string;
  ExchangeCode: string;
  // ExchangeOperatorId: null;
  // ExchangeOperatorCode: null;
  // ExchangeOperatorName: null;
  CurrencyNumericCode: number;
  CurrencyCode: string;
  CurrencyName: string;
  CurrencyDecimals: number;
  LocationId: string;
  LocationName: string;
  LocationNaturalId: string;
}
