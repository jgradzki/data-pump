export interface RainPricesInterface {
  /**
   * The Ticker
   */
  Ticker?: string;
  /**
   * The Market Maker buy (if present)
   */
  MMBuy?: number;
  /**
   * The Market Maker sell (if present)
   */
  MMSell?: number;
  /**
   * The price average of the material on the CI1 exchange
   */
  CI1Average?: number;
  /**
   * The amount of items available for the lowest asking price
   */
  CI1AskAmt?: number;
  /**
   * The lowest asking price
   */
  CI1AskPrice?: number;
  /**
   * The amount of items listed available for buy across all ads
   */
  CI1AskAvail?: number;
  /**
   * The amount of items requested for the highest bid price
   */
  CI1BidAmt?: number;
  /**
   * The highest bid price
   */
  CI1BidPrice?: number;
  /**
   * The amount of items requesting to buy across all ads
   */
  CI1BidAvail?: number;
  /**
   * The price average of the material on the NC1 exchange
   */
  NI1Average?: number;
  /**
   * The amount of items available for the lowest asking price
   */
  NI1AskAmt?: number;
  /**
   * The lowest asking price
   */
  NI1AskPrice?: number;
  /**
   * The amount of items listed available for buy across all ads
   */
  NI1AskAvail?: number;
  /**
   * The amount of items requested for the highest bid price
   */
  NI1BidAmt?: number;
  /**
   * The highest bid price
   */
  NI1BidPrice?: number;
  /**
   * The amount of items requesting to buy across all ads
   */
  NI1BidAvail?: number;
  /**
   * The price average of the material on the IC1 exchange
   */
  IC1Average?: number;
  /**
   * The amount of items available for the lowest asking price
   */
  IC1AskAmt?: number;
  /**
   * The lowest asking price
   */
  IC1AskPrice?: number;
  /**
   * The amount of items listed available for buy across all ads
   */
  IC1AskAvail?: number;
  /**
   * The amount of items requested for the highest bid price
   */
  IC1BidAmt?: number;
  /**
   * The highest bid price
   */
  IC1BidPrice?: number;
  /**
   * The amount of items requesting to buy across all ads
   */
  IC1BidAvail?: number;
}
