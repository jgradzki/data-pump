import { RainPricesInterface } from '@/interfaces/fioApi/RainPricesInterface';

export interface CurrentMaterialPrice extends RainPricesInterface {
  date: number;
}

export interface MaterialPriceSnapshot extends RainPricesInterface {
  '@timestamp': number;
}
