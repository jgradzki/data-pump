export interface GeneralConfigInterface {
  apiUrl: string;
  materialPricesSnapshotsInterval: number;
}
