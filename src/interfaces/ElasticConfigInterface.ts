export interface ElasticConfigInterface {
  port: number;
  host: string;
  requestTimeout: number;
  maxSockets: number;
  materialPricesIndexName: string;
  materialPricesSnapshotsIndexName: string;
  user?: string;
  password?: string;
}
